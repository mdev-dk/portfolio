const express = require('express')
const http = require('http')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const sass = require('node-sass')
const fs = require('fs')
const compression = require('compression')
const path = require('path')

const app = express()
const port = 3000
const logger = morgan('dev')

app.set('port')
app.use(logger)
app.use(bodyParser.json())
app.use(compression())
app.set('views', __dirname + '/views')
app.set('view engine', 'pug')

// Style compiling >
fs.readdirSync(path.resolve(__dirname, 'styles'))
  .filter(file =>  (file.indexOf('.') !== 0) && (file.slice(-5) === '.scss'))
  .forEach(file => {
    const outputPath = `styles/min/${file}.min.css`,
      outputMapPath = `${outputPath}.map`

    const sassResult = sass.renderSync({
      file: `${__dirname}/styles/${file}`,
      outputStyle: 'compressed',
      outFile: outputPath,
      sourceMap: true
    })

    fs.truncate(outputPath, 0, () => {
      fs.writeFile(outputPath, sassResult.css, () => {})
    })
    fs.truncate(outputMapPath, 0, () => {
      fs.writeFile(outputMapPath, sassResult.map, () => {})
    })
  })
// <

app.use('/media', express.static(__dirname + '/media'))
app.use('/styles', express.static(__dirname + '/styles'))

app.get('/', (req, res) => {
  res.render('home', {
  })
})

app.get('/pages', (req, res) => {
  res.render(req.query.file, {
  })
})

app.get('/renders', (req, res) => {
  res.render('includes/'+req.query.file)
})

const server = http.createServer(app)
server.listen(port)
